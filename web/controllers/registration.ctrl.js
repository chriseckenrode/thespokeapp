function RegisterCtrl(UserService, RegistrationService, $scope, ngToast) {
  var vm = this;
  angular.element(document).ready(function() {
    $("input[type='checkbox'], input[type='radio']").iCheck({
      checkboxClass: 'icheckbox_square-green'
    });
    $('ins').click(function() {
      vm.register.agree = !vm.register.agree;
      $scope.$digest();
    });
  });

  vm.register = {};

  vm.registerUser = function() {
    console.log(RegistrationService.validateRegistration(vm.register));
    var result = RegistrationService.validateRegistration(vm.register);
    if (!result.isValid) {
      vm.requiredIsEmpty = true;
      ngToast.create({
        className: 'warning',
        content: '<p>' + result.message + '</p>'
      });
    } else {
      vm.requiredIsEmpty = false;
      ngToast.create({
        className: 'success',
        content: '<p>' + result.message + '</p>'
      });
    }
  };
}

angular.module('thespoke').controller('RegistrationCtrl', RegisterCtrl);

RegisterCtrl.$inject = ['UserService', 'RegistrationService', '$scope', 'ngToast'];
