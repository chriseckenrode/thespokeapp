function HomeCtrl($scope) {
  angular.element(document).ready(function(){
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $scope.selectedCategory = 0;

    $scope.openNewListModal = function(){
      $('#new-list').modal('show');
    }

  });

}

angular.module('thespoke').controller('HomeCtrl', HomeCtrl);

// HomeCtrl.$inject = []
