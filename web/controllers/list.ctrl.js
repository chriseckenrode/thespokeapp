function ListCtrl($scope, $sce) {
  angular.element(document).ready(function(){
    $("[data-toggle=popover]").popover();
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $scope.listType = 0;

    $scope.items = [{title: "Die Hard", media: [{type:"amazon", src: "amazon.com"}, {type:"itunes", src: "itunes.com"}], user: "Danny", tomatoScore: "92%", IMDBScore: "8.6", userScore:"4/4"}, {title: "Die Hard", media: [{type:"amazon", src: "amazon.com"}, {type:"itunes", src: "itunes.com"}], user: "Danny", tomatoScore: "92%", IMDBScore: "8.6", userScore:"4/4"}, {title: "Die Hard", media: [{type:"amazon", src: "amazon.com"}, {type:"itunes", src: "itunes.com"}], user: "Danny", tomatoScore: "92%", IMDBScore: "8.6", userScore:"4/4"}, {title: "Die Hard", media: [{type:"amazon", src: "amazon.com"}, {type:"itunes", src: "itunes.com"}], user: "Danny", tomatoScore: "92%", IMDBScore: "8.6", userScore:"4/4"}];
  });
  $scope.placement = {
    selected: 'bottom'
  };

}

angular.module('thespoke').controller('ListCtrl', ListCtrl);

ListCtrl.$inject = ['$scope', '$sce'];
