function Register() {
  var that = this;
  this.validateRegistration = function(formData) {
    if (!formData.firstName || !formData.lastName || !formData.password || !formData.confirmPassword) {
      return {
        isValid: false,
        message: "Please fill out all of the required fields"
      };
    } else if (!formData.email){
      return {
        isValid: false,
        message: "Please enter a valid email address"
      };
    } else if (formData.password !== formData.confirmPassword) {
      return {
        isValid: false,
        message: "Your passwords do not match"
      };
    } else if (!formData.agree){
      return {
        isValid: false,
        message: "You must agree to the terms and conditions"
      };
    } else {
      return {
        isValid: true,
        message: "Registered successfully"
      };
    }
  };
}

angular.module('thespoke').service('RegistrationService', Register);

User.$inject = ['$http', '$q'];
