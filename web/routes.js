angular.module('thespoke').config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: '/views/home/home.html',
        controller: 'HomeCtrl'
      }).state('register', {
        url: '/register',
        templateUrl: '/views/register/register.html',
        controller: 'RegistrationCtrl'
      }).state('list', {
        url: '/list',
        templateUrl: '/views/list/list.html',
        controller: 'ListCtrl'
      });
  }
]);
