var express = require('express');
var app = express();
var PORT = process.env.PORT || 3000;

app.use('/views', express.static(process.cwd() + '/views'));
app.use('/styles', express.static(process.cwd() + '/styles'));
app.use('/images', express.static(process.cwd() + '/images'));
app.use('/web', express.static(process.cwd() + '/web'));
app.use('/vendor', express.static(process.cwd() + '/vendor'));

require('./server/routes/main')(app);

app.listen(PORT, function(err) {
  if (err) {
    console.error(err);
  } else {
    console.info("==> 🌎  Listening on port %s. Visit http://localhost:%s/ in your browser.", PORT, PORT);
  }
});
